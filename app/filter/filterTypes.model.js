import Model from '../utils/model'

const FilterTypes = Object.assign({}, Model, {
  default: {
    filters: [],
  },

  url: 'http://www.mocky.io/v2/5a0885e73200005a05137ff0',
})

export default FilterTypes
