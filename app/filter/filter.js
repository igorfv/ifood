import View from '../utils/view'
import Model from '../utils/model'

import template from './filter.dust'

const Filter = Object.assign({}, View, {
  template,
  model: Object.create(Model),

  start (initializationData) {
    View.start.call(this, initializationData)
    this.render()
  },
})

export default Filter
