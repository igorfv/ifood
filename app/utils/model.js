// eslint-disable-next-line no-undef
const browser = window

const Model = {

  default: null,

  _data: {},

  url: null,

  parse (response) {
    if (!response.ok) return {}
    return response.json()
  },

  set (data) {
    Object.assign(this._data, this._omitExtraData(data))
    return this._data
  },

  get (key) {
    const data = Object.assign({}, this.default, this._data)

    if (key) return data[key]
    return data
  },

  fetch (options = {}) {
    const fetchOptions = Object.assign(
      {
        method: 'GET',
      },
      options,
    )

    return browser.fetch(this._getResourceUrl(), fetchOptions)
      .then(response => this.parse(response))
      .then((data) => {
        this.set(data)
        return data
      })
  },

  _getResourceUrl () {
    if (this.url instanceof Function) return this.url()
    return this.url
  },

  // Remove keys that are not present on default
  _omitExtraData (data) {
    if (!this.default) return data

    const validKeys = Object.keys(this.default)
    const dataKeys = Object.keys(data)
    return dataKeys.reduce((result, key) => {
      if (!validKeys.includes(key)) return result

      return Object.assign(result, {
        [key]: data[key],
      })
    }, {})
  },
}

export default Model
