import dust from 'dustjs-linkedin'

// eslint-disable-next-line no-undef
const browser = window

const View = {

  el: null,
  template: null,
  model: null,

  components: null,
  regions: null,
  initializedComponents: {},

  start (initializationData = {}) {
    if (this.model) {
      this.model.set(initializationData)
    }

    this._initializeComponents(initializationData)
    this.pageBoot(initializationData)
  },

  render () {
    if (!this.template || !this.el) return

    dust.render(this.template, this.model.get(), (err, result) => {
      if (err) {
        throw new Error(err)
      }

      this.el.innerHTML = result
    })
  },

  pageBoot () {},

  _initializeComponents (initializationData) {
    if (!this.components || !this.regions) return

    const componentNames = Object.keys(this.components)
    componentNames.forEach(name => this._initializeComponent(name, initializationData))
  },

  _initializeComponent (componentName, initializationData) {
    if (!componentName || !this.components || !this.regions) return

    const component = Object.create(this.components[componentName], {
      el: { value: browser.document.querySelector(this.regions[componentName]) },
    })

    component.start(initializationData)
    this.initializedComponents[componentName] = component
  },
}

export default View
