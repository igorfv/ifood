const Mediator = {

  components: {},
  models: {},

  events: [],

  start ({ components = {}, models = {} }) {
    this.eventBus = this
    this.components = components
    this.models = models

    this._setEventBus(this.components)
    this._setEventBus(this.models)

    this.mediate()
    this.initialize()
  },

  mediate () {},
  initialize () {},

  on (name, callback) {
    this.events.push({ name, callback })
    return this
  },

  trigger (name, data) {
    this.events.forEach((event) => {
      if (event.name === name) {
        event.callback(data)
      }
    })

    return this
  },

  _setEventBus (items) {
    Object.keys(items)
      .forEach((key) => {
        // eslint-disable-next-line no-param-reassign
        items[key].eventBus = this.eventBus
        return key
      })
  },
}

export default Mediator
