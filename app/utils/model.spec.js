import Model from './model'

const mockData = {
  foo: true,
  bar: 1,
  doh: 'homer',
}

describe('Model', () => {
  describe('get/set', () => {

    test('omit data not present on default', () => {
      const model = Object.create(Model, {
        default: {
          value: {
            foo: null,
            bar: null,
          }
        },
      })

      model.set(mockData)

      expect(model.get()).toEqual({
        foo: true,
        bar: 1,
      })
    })

    test('doesn\'t omit any data if default is null', () => {
      const model = Object.create(Model)

      model.set(mockData)

      expect(model.get()).toEqual(mockData)
    })

    test('get a sinle data attribute', () => {
      const model = Object.create(Model)

      model.set(mockData)

      expect(model.get('doh')).toEqual('homer')
    })

  })
})
