import View from './view'

const FakeModel = {
  _data: {},
  set (data) {
    this._data = data
  },
  get () {
    return this._data
  },
}

const ViewWithModel = Object.assign({}, View, {
  model: FakeModel,
})

describe('View', () => {
  describe('start', () => {
    test('set model data with initialization data', () => {
      const view = Object.create(ViewWithModel)

      view.start({
        foo: true,
        bar: 1,
      })

      expect(view.model.get()).toEqual({
        foo: true,
        bar: 1,
      })
    })

    test('initialize all components with the ', () => {
      const view = Object.create(ViewWithModel, {
        components: {
          value: {
            foo: ViewWithModel,
            bar: ViewWithModel,
          }
        },
        regions: {
          value: {
            foo: '.foo',
            bar: '.bar',
          }
        }
      })

      const initializationData = {
        foo: true,
        bar: 1,
      }

      view.start(initializationData)

      const foo = view.initializedComponents.foo
      const bar = view.initializedComponents.bar

      expect(foo.model.get()).toEqual(initializationData)
      expect(foo.model.get()).toEqual(initializationData)
    })

  })
})
