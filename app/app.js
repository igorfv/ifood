import View from './utils/view'
import mediator from './app.mediator'

import Filter from './filter/filter'
import FilterTypesModel from './filter/filterTypes.model'

import PlaylistAuthModel from './playlist/playlistAuth.model'

const App = Object.assign({}, View, {

  mediator,

  components: {
    filter: Filter,
  },

  regions: {
    filter: '.js-filter',
  },

  models: {
    filterTypes: Object.create(FilterTypesModel),
    playlistAuth: Object.create(PlaylistAuthModel),
  },

  pageBoot (initializationData) {
    this._initializeModels(this.models, initializationData)

    this.mediator.start({
      components: this.initializedComponents,
      models: this.models,
    })
  },

  _initializeModels (models, initializationData) {
    Object.keys(models)
      .forEach(name => models[name].set(initializationData))
  },
})

// eslint-disable-next-line no-undef
window.App = App
export default App
