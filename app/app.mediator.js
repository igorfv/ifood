import Mediator from './utils/mediator'

const AppMediator = Object.assign({}, Mediator, {

  mediate () {
    this
      .on('auth:new_token', () => this.models.playlistAuth.fetch().then((response) => {
        console.log(response)
      }))
      .on('auth:unauthorized', () => console.log('client id and secret are wrong'))
      .on('accessToken:denied', () => this.trigger('auth:new_token'))
      .on('accessToken:new', () => console.log('refresh playlist'))
  },

  initialize () {
    // Get new access token
    this.trigger('auth:new_token')
  },

})

export default Object.create(AppMediator)
