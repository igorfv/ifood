import Model from '../utils/model'

// eslint-disable-next-line no-undef
const browser = window

const FilterTypes = Object.assign({}, Model, {
  default: {
    client_id: null,
    client_secret: null,
    grant_type: 'client_credentials',
  },

  url: 'https://accounts.spotify.com/api/token',

  fetch () {
    const { grant_type, client_id, client_secret } = this.get()

    const headers = new browser.Headers()

    // eslint-disable-next-line camelcase
    const base64AuthCode = browser.btoa(`${client_id}:${client_secret}`)
    headers.append('Authorization', `Basic ${base64AuthCode}`)
    headers.append('Content-Type', 'application/json')

    return Model.fetch.call(this, {
      method: 'POST',
      headers,
      body: JSON.stringify({ grant_type }),
    })
  },
})

export default FilterTypes
