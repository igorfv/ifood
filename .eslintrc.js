module.exports = {
  extends: 'airbnb-base',
  plugins: ['import'],
  rules: {
    'semi': ['error', 'never'],
    // Avoid functions with multiple exists
    'complexity': ['error', 5],
    // Required to avoid no-semicolon JS gotchas
    'no-unexpected-multiline': 'error',
    // By enforcing this, we ensure shorthand function declarations remain searchable
    'space-before-function-paren': ['error', 'always'],
    // Since ES2015, function names are not required for debugging
    'func-names': ['error', 'never'],
    // Allow empty comment lines
    'spaced-comment': ['off'],
    // Allow underscore prefix to indicate private methods
    'no-underscore-dangle': ['off'],
    // Allows unused variables when prefixed by an `underscore`
    'no-unused-vars': ['error', { 'argsIgnorePattern' : '_' }],
    // Only warn for unused expressions, mainly because of specs
    'no-unused-expressions': ['warn'],
  },
}
