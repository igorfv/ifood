const path = require('path')
const webpack = require('webpack')
const CopyPlugin = require('copy-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const extractCssPlugin = new ExtractTextPlugin('../css/[name].css')
const extractHtmlPlugin = new ExtractTextPlugin('../[name].html')

const config = {
  outputPath: 'dist',
  cdnPath: '../img',
}

module.exports = {
  entry: {
    index: ['./app/index.html'],
    app: [
      './app/app.js',
      './app/app.scss',
    ],
    setup: ['./app/setup'],
  },

  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, config.outputPath, 'js'),
  },

  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: /\.crit.scss$/,
        use: extractCssPlugin.extract({
          use: [
            'raw-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  require('autoprefixer')({ browsers: 'last 2 versions', remove: false }),
                  require('cssnano')(),
                ],
              },
            },
            {
              loader: 'sass-loader',
              options: { data: `$cdn-path: "${config.cdnPath}";` },
            },
          ],
        }),
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        include: path.resolve(__dirname, 'app'),
        enforce: 'pre',
        use: ['eslint-loader'],
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        include: path.resolve(__dirname, 'app'),
        use: ['babel-loader'],
      },

      {
        test: /\.dust$/,
        use: {
          loader: 'dust-loader',
          options: {
            rootDir: 'app'
          },
        },
      },

      // FIX: Replace raw html files for a real template engine
      {
        test: /\.html$/,
        exclude: /index.html$/,
        include: path.resolve(__dirname, 'app'),
        use: ['raw-loader'],
      },
      {
        test: /\index.html$/,
        use: extractHtmlPlugin.extract({
          use: [
            {
              loader: 'html-loader',
              options: {
                attrs: false,
                interpolate: 'require',
              }
            },
          ],
        }),
      },
    ],
  },


  plugins: [
    extractHtmlPlugin,
    extractCssPlugin,
    new CopyPlugin([
      {
        context: path.resolve(__dirname, 'app'),
        from: '**/*.+(jpg|png|svg)',
        to: '../images/[path][name]',
      },
    ]),
    new webpack.ProvidePlugin({
      dust: 'dustjs-linkedin',
    }),
  ],
}
