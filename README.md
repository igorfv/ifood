# Spotifood

## Dependencies
  - Install NodeJS 6+
  - Install Yarn
  - Install dependencies with `yarn install`

## How to run (local)
  - Run `yarn start` to start a local server
  - Open http://localhost:8080/

## How to deploy
  The assets file are deployed to an S3 as soon as they are merged to the master. CircleCi does all the work.

  > Note that it's not ready to deploy as the circle.yml is configured with a fake S3 bucket and there's no CircleCi for this project

## How to contribute
  - Run `yarn start` to start a local server with live reload
  - Build the assets with `yarn watch`, it will watch for file changes
  - Run the tests with `yarn test`, it will watch for file changes
  - Open http://localhost:8080/
